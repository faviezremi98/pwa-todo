console.log('Service worker loaded');
const excludeFromCache = [
  'https://www.google.com/images/phd/px.gif',
  'http://localhost:8081/data/spacex.json',
  'http://localhost:3000'
];
const cacheVersion = 'v3';
self.addEventListener('install', function(event) {
  event.waitUntil(
    caches.open(cacheVersion)
      .then(function (cache) {
          cache.addAll([
          './assets/styles/style.css',
          './index.html',
          './src/views/edit.js',
          './src/views/create.js',
          './src/views/home.js',
          './src/app.js',
          './service-worker.js',
          './src/network.js',
          './src/idb.js'
        ])
        .then(function(resp){
          console.log("Page mis en cache")
        })
      })
  );
});
self.addEventListener('activate', function(event) {
  event.waitUntil(clients.claim());
});

self.addEventListener('message', function (event) {
  if (event.data.type === 'SKIP_WAITING') {
    self.skipWaiting();
  }
});