import page from 'page';
import checkConnectivity from './network.js';
import { fetchTodos, fetchTodo, createTodo, deleteTodo, updateTodo } from './api/todo.js';
import { setTodos, setTodo, getTodos, unsetTodo, getTodo } from './idb.js';

checkConnectivity({});
document.addEventListener('connection-changed',async e => {
  document.offline = !e.detail;

  if (!document.offline) {

    getTodos().then(function(resp){
        resp.forEach(function(element){
          if(element.synced === 'false' && element.updated === 'true'){
            element.synced = "true";
            createTodo(element);
            setTodo(element);
            window.location.reload();
          } else if(element.synced === 'false' && element.updated === 'false') {
            element.synced = "true";
            element.updated = "true";
            updateTodo(element);
            setTodo(element);
            window.location.reload();
          }
        });
     });
  } 
});

const app  = document.querySelector('#app .outlet');

fetch('/config.json')
  .then((result) => result.json())
  .then(async (config) => {
    console.log('[todo] Config loaded !!!');
    window.config = config;

    page('/', async () => {
      const module = await import('./views/home.js');
      const Home = module.default;

      const ctn = app.querySelector('[page="Home"]');
      const homeView = new Home(ctn);

      let todos = [];
      if (!document.offline && navigator.onLine) {
        const data = await fetchTodos();
        todos = await setTodos(data);
      } else {
        todos = await getTodos() || [];
      }

      homeView.todos = todos;
      homeView.renderView();
      displayPage('Home');
    
      document.addEventListener('delete-todo', async ({ detail }) => {
        if (!document.offline && navigator.onLine === true) {
          const result = await deleteTodo(detail.id);
          if (result !== false) {
            // If we successfuly get a result from API
            // Get the updated todo list
            const todo = await unsetTodo(detail.id);
            // Rerender the template
            return document.dispatchEvent(new CustomEvent('render-view', { detail: todo }));
          }
          // In case of an error
          detail.deleted = 'true';
        }
      });
    });

      page('/create', async () => {
        const module = await import('./views/create.js');
        const Create = module.default;
  
        const ctn = app.querySelector('[page="Create"]');
        const createView = new Create(ctn);
        let tasks = [{id: 'task-0', value: '', state: false}];
        
        createView.tasks = tasks;

        createView.renderView();
        displayPage('Create');
  
        // Create todo
        document.addEventListener('create-todo', async ({ detail: todo }) => {
          await setTodo(todo);
          if (!document.offline && navigator.onLine === true) {
            todo.synced = "true";
            const result = await createTodo(todo);

            const fetch = await fetchTodos;
            let todos = [];
              
              
            fetch().then((resp) => {
             todos = resp;
            });
            await setTodos(todos);
              
            if (result !== false) {

              window.location.href = '/';
            }

            todo.synced = 'false';
            // Rerender the template
          }
          window.location.href = '/';
        });
    });

    page('/todos/:id', async (req) =>  {
        const module = await import('./views/edit');
        const Edit = module.default;
        
        const ctn = app.querySelector('[page="Edit"]');
        const editview = new Edit(ctn);
        let todo = null;
        if (document.offline && !navigator.onLine) {
          todo = await fetchTodo(req.params.id);
        } else {
          todo = await getTodo(req.params.id);
        }
        editview.todo = todo;
        
        editview.renderView();
        displayPage('Edit');

        document.addEventListener('update-todo', async ({ detail: todo }) => {
         await setTodo(todo);

          if (!document.offline && navigator.onLine === true) {
            const result = await updateTodo(todo)
            await setTodo(todo)
            if(result !== false)
            {
              const fetch = await fetchTodos;
              let todos = [];
              
              
              fetch().then((resp) => {
                todos = resp;
              });
              await setTodos(todos);
              window.location.href = '/'
            } 
            // Rerender the template
          } else {
            todo.synced = 'false';
            todo.updated = 'false';
            await setTodo(todo);

          }
          window.location.href = '/';
        });
    });

  

    page();
});

  function displayPage(page) {
    const skeleton = document.querySelector('#app .skeleton');
    skeleton.removeAttribute('hidden');
    const pages = app.querySelectorAll('[page]');
    pages.forEach(page => page.removeAttribute('active'));
    skeleton.setAttribute('hidden', 'true');
    const p = app.querySelector(`[page="${page}"]`);
    p.setAttribute('active', true);
  }