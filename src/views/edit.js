import { render, html } from 'lit-html';
import 'lit-icon';

export default class Edit {
    constructor(page) {
        this.page = page;
        this.properties = { 
            todo: ""
        }
    }

    set todo(value) {
        this.properties.todo = value;
    }
    
    get todo() {
        return this.properties.todo;
    }

    template()
    {
        return html `
        <section class="h-full">
            <main class="todolist px-4 pb-20">
                <div class="w-full pt-4">
                    <form @submit="${this.handleForm.bind(this)}" id="editTodo" class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
                        <div class="mb-4">
                            <label class="flex items-center justify-between block text-gray-700 text-sm font-bold mb-2" for="username">
                              Titre de la TodoList
                            </label>
                            <input 
                            .value="${this.properties.todo.title}" 
                            autocomplete="off" 
                            class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" 
                            id="title" 
                            type="text" 
                            placeholder="Ménage"
                            name="Titre"
                            @input="${e => this.properties.title = e.target.value}">
                        </div>
                        <div class="mb-4">
                            <label class="flex items-center justify-between block text-gray-700 text-sm font-bold mb-2" for="username">
                              Description
                            </label>
                            <input 
                            .value="${this.properties.todo.description}" 
                            autocomplete="off" 
                            class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" 
                            id="description" 
                            type="text" 
                            placeholder="Ménage"
                            name="Titre"
                            @input="${e => this.properties.description = e.target.value}">
                        </div>
                        ${this.properties.todo.tasks.map(task => html`
                        <div class="mb-4">
                            <label class="flex items-center justify-between block text-gray-700 text-sm font-bold mb-2" for="username">
                              Tâche ${task.id.split('-')[1]}
                            </label>
                            <input 
                            .value="${task.value}" 
                            autocomplete="off" 
                            class="shadow appearance-none border rounded w-90 py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" 
                            id="${task.id}"
                            type="text" 
                            placeholder="Nettoyer la cuisine"
                            name="Tâche"
                            @input="${e => task.value = e.target.value}">
                            ${task.state ?
                                html `
                                <input class="mr-2 leading-tight" 
                                type="checkbox" 
                                checked
                                .value=${task.state}
                                @change=${() => task.state = !task.state}/>` :
                                html`
                                <input type="checkbox"
                                .value=${task.state}
                                @change=${() => task.state = !task.state}/>`
                            }
                        </div>
                        `)}
                        <div class="flex items-center justify-center flex-col">
                            <span @click="${this.handleAddTask.bind(this)}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full" id="addTask">
                                +
                            </span>
                            <label class="block text-gray-700 text-sm font-bold mb-2" for="password">
                                Ajouter une tâche
                            </label>
                        </div>
                        <div class="flex items-center justify-center">
                            <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit" id="addTodo">
                                Créer ma todo
                            </button>
                        </div>
                    </form>
                </div>
            </main>
        </section>
        `;
    }

    renderView() {
        const view = this.template();
        render(view, this.page);
    }

    addError(arrayErrors)
    {
        arrayErrors.map((input) => {
            let element = document.getElementById(input);
            let classList = element.classList;
            classList.add('border-red-500', 'border');
            let p = document.createElement('p');
            let numberTask = null;
            if(input.substring(0, 4) === 'task')
            {
                numberTask = input.substring(input.length - 1);
            }
            let text = document.createTextNode(element.getAttribute('name') + ' ' + (numberTask !== null ? numberTask+' ' : '') + 'is required')
            p.appendChild(text);
            let parent = element.parentElement;
            parent.appendChild(p);
            p.classList.add('text-red-500', 'text-xs', 'italic')
        })
    }

    handleForm(e) {
        e.preventDefault();
        let arrayErrors = [];

        for(var myPropertyName in this.properties.todo)
        {
            if(this.properties.todo[myPropertyName] === '' && myPropertyName !== 'tasks')
            {
                arrayErrors.push(myPropertyName);
            } else if (myPropertyName === 'tasks')
            {
                this.properties.todo.tasks.forEach(element => {
                    if(element.value === '')
                    {
                        arrayErrors.push(element.id);
                    }
                })
            }
        }

        if(arrayErrors.length !== 0)
        {
            return this.addError(arrayErrors);
        }


        const todo = {
            ...this.properties.todo,
            title: this.properties.todo.title,
            tasks: this.properties.todo.tasks,
            description: this.properties.todo.description,
        }

        const event = new CustomEvent('update-todo', { detail: todo });
        document.dispatchEvent(event);
    }

    handleAddTask()
    {
        let newTasks = this.properties.todo.tasks;
        let numberTasks = newTasks.length;
        newTasks.push({id:"task-"+numberTasks, value: '', state: false});
        this.tasks = newTasks
        this.renderView();
    }
}