import { render, html } from 'lit-html';
import '../component/todo-card.js';
import 'lit-icon';

export default class Create {
    constructor(page) {
        this.page = page;
        this.properties = {
            title: "",
            desc: "",
            tasks: []
        }

        this.taskNumber = 0;
        this.renderView();
    }

    set tasks(value) {
        this.properties.tasks = value;
    }
    
    get tasks() {
        return this.properties.tasks;
    }
    
    template() {
        return html`
        <section class="h-full>
            <main class="todolist px-4 pb-20">
                <div class="w-full pt-4">
                    <a href="/" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full">←</a>
                    <form @submit="${this.handleForm.bind(this)}" id="addTodo" class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
                        <div class="mb-4">
                            <label class="flex items-center justify-between block text-gray-700 text-sm font-bold mb-2" for="username">
                              Titre de la TodoList
                            </label>
                            <input 
                            .value="${this.properties.title}" 
                            autocomplete="off" 
                            class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" 
                            id="title" 
                            type="text" 
                            placeholder="Ménage"
                            name="Titre"
                            @input="${e => this.properties.title = e.target.value}">
                        </div>
                        <div class="mb-6">
                           <label class="block text-gray-700 text-sm font-bold mb-2" for="password">
                                Description
                            </label>
                            <input
                            .value="${this.properties.desc}"
                            @input="${e => this.properties.desc = e.target.value}"
                            autocomplete="off" 
                            class="shadow appearance-none rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline" 
                            id="desc" 
                            type="text" 
                            name="Description"
                            placeholder="Ménage de Printemps">
                        </div>
                        <div id="alltasks">
                        ${this.tasks.map(tasks => html`
                        <div class="mb-6">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="password">
                                    Tâche
                                </label>
                                <input 
                                autocomplete="off"
                                class="shadow appearance-none rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline" 
                                id="${tasks.id}"
                                .value="${tasks.value}"
                                @input="${e => tasks.value = e.target.value}"
                                type="text"
                                name="Tâche"
                                placeholder="Ranger ma chambre...">
                            </div>
                            `)}

                            
                        </div>
                        <div class="flex items-center justify-center flex-col">
                            <span @click="${this.handleAddTask.bind(this)}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full" id="addTask">
                                +
                            </span>
                            <label class="block text-gray-700 text-sm font-bold mb-2" for="password">
                                Ajouter une tâche
                            </label>
                        </div>
                        <div class="flex items-center justify-center">
                            <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit" id="addTodo">
                                Créer ma todo
                            </button>
                        </div>
                    </form>
                </div>
            </main>
        </section>
        `;
    }
    
    renderView() {
        const view = this.template();
        render(view, this.page);
    }

    addError(arrayErrors)
    {
        arrayErrors.map((input) => {
            let element = document.getElementById(input);
            let classList = element.classList;
            classList.add('border-red-500', 'border');
            let p = document.createElement('p');
            let numberTask = null;
            if(input.substring(0, 4) === 'task')
            {
                numberTask = input.substring(input.length - 1);
            }
            let text = document.createTextNode(element.getAttribute('name') + ' ' + (numberTask !== null ? numberTask+' ' : '') + 'is required')
            p.appendChild(text);
            let parent = element.parentElement;
            parent.appendChild(p);
            p.classList.add('text-red-500', 'text-xs', 'italic')
        })
    }

    handleForm(e)
    {
        e.preventDefault();
        let arrayErrors = [];

        for(var myPropertyName in this.properties)
        {
            if(this.properties[myPropertyName] === '' && myPropertyName !== 'tasks')
            {
                arrayErrors.push(myPropertyName);
            } else if (myPropertyName === 'tasks')
            {
                this.properties.tasks.forEach(element => {
                    if(element.value === '')
                    {
                        arrayErrors.push(element.id);
                    }
                })
            }
        }

        if(arrayErrors.length !== 0)
        {
            return this.addError(arrayErrors);
        }

        const todo = {
            id: Date.now(),
            title: this.properties.title,
            description: this.properties.desc,
            synced: "false",
            updated: "true",
            done: "false",
            deleted: "false",
            date: Date.now(),
            tasks: this.properties.tasks
        }

        const event = new CustomEvent('create-todo', { detail: todo });
        document.dispatchEvent(event);
    }

    handleAddTask()
    {
        let newTasks = this.tasks;
        this.taskNumber++;
        newTasks.push({id:"task-"+this.taskNumber, value: '', state: false});
        this.tasks = newTasks
        this.renderView();
    }
}
//                            <p class="text-red-500 text-xs italic">Please choose a password.</p> // border-red-500
