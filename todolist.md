# Todo 

## TD

Réaliser une PWA Todo list avec les fonctionnalité suivantes:
* Ajouter une tache avec un titre et une description et un état réalisé - Done
* Sauvegarder les todos en DB - Done
* Créer/Lire les todos offline - Done
* Synchroniser les todos créés/modifié hors ligne à la recupération de la connexion Done 
* Bonus : Avoir une page de détail de todo - Done

## Libs
* JSON-server (https://github.com/typicode/json-server)
* IndexedDB (https://github.com/jakearchibald/idb)
* Tailwind CSS (https://tailwindcss.com/)
* Page.js (https://visionmedia.github.io/page.js/)
  
